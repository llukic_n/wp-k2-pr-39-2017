﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zadatak1.Models;

namespace Zadatak1.Controllers
{
    public class AutentikacijaController : Controller
    {
        // GET: Autentikacija
        [HttpPost]
        public ActionResult Index(string username,string password)
        {
            List <Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            
            foreach (Korisnik k in korisnici)
            {
                if (k.Username.Equals(username) && k.Password.Equals(password))
                {
                    Session["korisnik"] = k;
                    k.LoggedIn = true;
                    ViewBag.korisnik = k;
                    return RedirectToAction("Index","Home");
                }              
            }
            ViewBag.poruka = "Pogresno korisnicko ime ili lozinka";
            return View("Index");
        }
       
        public ActionResult Index1()
        {
            return View("Index");
        }
    }
}