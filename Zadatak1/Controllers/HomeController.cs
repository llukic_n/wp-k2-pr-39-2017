﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Zadatak1.Models;

namespace Zadatak1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Ulazna()
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            if (k == null)
                return RedirectToAction("Index1", "Autentikacija");
            else
                return View("Index");
        }

        public ActionResult Index()
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            List<Kupovina> kupovina = (List<Kupovina>)HttpContext.Application["kupovine"];
            ViewBag.najpomocnija = kupovina;
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            ViewBag.wohnungen = stanovi;
            if (k == null)
            {
                Session["Ulogovan"] = null;
            }
            else
            {
                if (k.Uloga.Equals(ulogee.ADMINISTRATOR))
                    Session["Ulogovan"] = "Admin";
                else
                    Session["Ulogovan"] = "Korisnik";
            }
            return View();
        }

        [HttpPost]
        public ActionResult Sortiraj(string sortTip,string uzlsil)
        {
            Stan pomocna;
            Korisnik k = (Korisnik)Session["korisnik"];
            //if (k == null || !k.LoggedIn)
            //return RedirectToAction("Index1", "Autentikacija");
            ViewBag.korisnik = k;
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            if (sortTip == "CENA")
            {
                for (int i = 0; i < stanovi.Count - 1; i++)
                {
                    for (int j = i + 1; j < stanovi.Count; j++)
                    {
                        if (uzlsil == "RASTUCI")
                        {
                            if (stanovi[i].Cena > stanovi[j].Cena)
                            {
                                pomocna = stanovi[i];
                                stanovi[i] = stanovi[j];
                                stanovi[j] = pomocna;
                            }

                        }
                        else
                        {
                            if (stanovi[i].Cena < stanovi[j].Cena)
                            {
                                pomocna = stanovi[i];
                                stanovi[i] = stanovi[j];
                                stanovi[j] = pomocna;
                            }
                        }
                    }
                }

                ViewBag.wohnungen = stanovi;
                return View("Index");
            }
            else if (sortTip == "TIP")
            {
                for (int i = 0; i < stanovi.Count - 1; i++)
                {
                    for (int j = i + 1; j < stanovi.Count; j++)
                    {
                        if (uzlsil == "RASTUCI")
                        {
                            if (stanovi[i].Tip_stana > stanovi[j].Tip_stana)
                            {
                                pomocna = stanovi[i];
                                stanovi[i] = stanovi[j];
                                stanovi[j] = pomocna;
                            }
                        }
                        else
                        {
                            if (stanovi[i].Tip_stana < stanovi[j].Tip_stana)
                            {
                                pomocna = stanovi[i];
                                stanovi[i] = stanovi[j];
                                stanovi[j] = pomocna;
                            }
                        }
                    }
                }

                ViewBag.wohnungen = stanovi;
                return View("Index");
            }
            else
            {
                for (int i = 0;i<stanovi.Count-1;i++)
                {
                    for (int j = i + 1;j<stanovi.Count;j++ )
                    {
                        byte[] prvi = Encoding.ASCII.GetBytes(stanovi[i].Grad);
                        byte[] drugi = Encoding.ASCII.GetBytes(stanovi[j].Grad);
                        if (uzlsil == "RASTUCI")
                        {
  
                            if (prvi[0] > drugi[0])
                            {
                                //Console.WriteLine(stanovi[i].Grad[0]);
                                pomocna = stanovi[i];
                                stanovi[i] = stanovi[j];
                                stanovi[j] = pomocna;
                            }

                        }
                        else
                        {
                            if (prvi[0] < drugi[0])
                            {
                                pomocna = stanovi[i];
                                stanovi[i] = stanovi[j];
                                stanovi[j] = pomocna;
                            }
                        }
                    }

                    
                }
                ViewBag.wohnungen = stanovi;
                return View("Index");
            }
            
        }

        [HttpPost]
        public ActionResult PretraziPoGradu(string searchGrad)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            ViewBag.korisnik = k;
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            List<Stan> povratna = new List<Stan>();

            foreach (Stan s in stanovi)
            {
                if (s.Grad == searchGrad)
                    povratna.Add(s);
            }
            ViewBag.wohnungen = povratna;
            return View("Index");
        }

        [HttpPost]
        public ActionResult PretraziPoTipu(Tip searchTip)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            ViewBag.korisnik = k;
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            List<Stan> povratna = new List<Stan>();

            foreach (Stan s in stanovi)
            {
                if (s.Tip_stana == searchTip)
                    povratna.Add(s);
            }
            ViewBag.wohnungen = povratna;
            return View("Index");
        }

        [HttpPost]
        public ActionResult PretraziPoCeni(int donja,int gornja)
        {
            Korisnik k = (Korisnik)Session["korisnik"];
            ViewBag.korisnik = k;
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            List<Stan> povratna = new List<Stan>();

            foreach (Stan s in stanovi)
            {
                if (s.Cena >= donja && s.Cena <= gornja)
                {
                    povratna.Add(s);
                }
            }
            ViewBag.wohnungen = povratna;
            return View("Index");
        }

        public ActionResult PrikaziSveKupce()
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            ViewBag.customers = korisnici;
            return View();
        }

        [HttpPost]
        public ActionResult Obrisi(string username)
        {
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            foreach (Korisnik k in korisnici.ToList())
            {
                if (k.Username == username)
                {
                    korisnici.Remove(k);
                }
            }

            ViewBag.customers = korisnici;
            return RedirectToAction("PrikaziSveKupce","Home");
        }

        
       
    }
}