﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zadatak1.Models;

namespace Zadatak1.Controllers
{
    public class NoviController : Controller
    {
        // GET: Novi
        [HttpPost]
        public ActionResult Index(Stan s)
        {
            if (s.Drzava.Length < 3)
            {
                ViewBag.message = "Drzava treba da ima minimalno 3 karaktera";
                return View("Index");
            }
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            if (stanovi.Contains(s))
            {
                ViewBag.message = "Ovaj stan vec postoji u listi";
                return View("Index");
            }

            Storage.upisiStan(s);
            stanovi.Add(s);
            return RedirectToAction("Index","Home");
        }

        public ActionResult Index1()
        {
            return View("Index");
        }

        [HttpPost]
        public ActionResult Modifikuj(int id)
        {
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            foreach (Stan s in stanovi)
            {
                if (s.Id == id)
                {
                    ViewBag.ModStan = s;
                    stanovi.Remove(s);
                    return View("Pojedinacan");
                }
            }

            return RedirectToAction("Index","Home");

        }

        [HttpPost]
        public ActionResult Modifikuj1(Stan s)
        {
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            stanovi.Add(s);
            //Storage.upisiStan(s);
            Storage.upisiSveStanove(stanovi);

            return RedirectToAction("Index","Home");
        }

        [HttpPost]
        public ActionResult Izaberi(int id)
        {
            //Session["kupljen"] = null;
            //Korisnik k = (Korisnik)Session["korisnik"];
            List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
            foreach (Stan s in stanovi)
            {
                if ((s.Id == id) && s.NaProdaju)
                {
                    ViewBag.wohnung = s;
                    //Session["kupljen"] = "ne";
                    return View("Pojedinacan1");
                }
            }
            //ViewBag.povratna = "Stan je vec prodat!";
            //Session["kupljen"] = "da";
            return View("KupljenStan");
        }

        [HttpPost]
        public ActionResult KupiStan(int id)
        {
            
                List<Stan> stanovi = (List<Stan>)HttpContext.Application["stanovi"];
                List<Kupovina> kupovina = (List<Kupovina>)HttpContext.Application["kupovine"];
                Korisnik k = (Korisnik)Session["korisnik"];
                foreach (Stan s in stanovi)
                {
                    if (s.Id == id)
                    {
                        s.NaProdaju = false;
                        Kupovina kup = new Kupovina(k, s, DateTime.Now, s.Cena);
                        kupovina.Add(kup);
                        ViewBag.korisnik = k;
                        ViewBag.kupovine = kupovina;
                        return View("IstorijaKupovine");
                    }
                }
                return View("Pojedinacan1");
   
        }

        public ActionResult MojaKorpa()
        {
            List<Kupovina> kupovina = (List<Kupovina>)HttpContext.Application["kupovine"];
            Korisnik k = (Korisnik)Session["korisnik"];
            ViewBag.korisnik = k;
            List<Kupovina> korpa = new List<Kupovina>();

            foreach (Kupovina kup in kupovina)
            {
                if (kup.Kupac.Username == k.Username)
                {
                    korpa.Add(kup);
                }
            }
            if (korpa.Count == 0)
            {
                Session["korpetina"] = "prazna";
                return View("MojaKorpa");
            }
            else
            {
                Session["korpetina"] = "puna";
                ViewBag.novaKorpa = korpa;
                return View("MojaKorpa");
            }
        }

    }
}