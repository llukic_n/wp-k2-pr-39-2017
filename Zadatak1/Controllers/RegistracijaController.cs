﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Zadatak1.Models;

namespace Zadatak1.Controllers
{
    public class RegistracijaController : Controller
    {
        // GET: Registracija
        /*public ActionResult Index()
        {
            return View();
        }*/

        [HttpPost]
        public ActionResult Index(Korisnik korisnik)
        {
            if (korisnik.Username == null || korisnik.Password == null || korisnik.Ime == null || korisnik.Prezime == null || korisnik.Pol == null || korisnik.Email == null || korisnik.datum_rodjenja == null ||korisnik.Uloga == null)
            {
                ViewBag.svapolja = "Moraju biti popunjena sva polja";
                return View("Index");
            }
            if (korisnik.Password.Length < 3)
            {
                ViewBag.svapolja = "Password mora imati najmanje 3 karaktera";
                return View("Index");
            }
            if (korisnik.Username.Length < 8)
            {
                ViewBag.svapolja = "Korisnicko ime mora imati najmanje 8 karaktera";
                return View("Index");
            }
            List<Korisnik> korisnici = (List<Korisnik>)HttpContext.Application["korisnici"];
            if (korisnik.Uloga == Zadatak1.Models.ulogee.ADMINISTRATOR)
            {
                ViewBag.adminova = "Administratori se ne mogu dodati!";
                return View();
            }
            foreach (Korisnik k in korisnici)
            {
                if (k.Username == korisnik.Username)
                {
                    ViewBag.poruka = $"Korisnik sa korisnickim imenom {korisnik.Username} vec postoji";
                    return View();
                }
            }
            Storage.upisiKorisnika(korisnik);
            korisnici.Add(korisnik);
            Session["korisnik"] = korisnik;
            return RedirectToAction("Index1", "Autentikacija");
        }

        public ActionResult Reg()
        {
            return View("Index");
        }

        
    }
}