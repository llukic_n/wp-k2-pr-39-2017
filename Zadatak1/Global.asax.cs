﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Zadatak1.Models;

namespace Zadatak1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            List<Korisnik> korisnici = Storage.UcitajKorisnika("~/App_Data/users.txt");
            HttpContext.Current.Application["korisnici"] = korisnici;

            List<Stan> stanovi = Storage.UcitajStanove("~/App_Data/stanovi.txt");
            HttpContext.Current.Application["stanovi"] = stanovi;

            List<Kupovina> kupovina = new List<Kupovina>();
            HttpContext.Current.Application["kupovine"] = kupovina;

        }
    }
}
