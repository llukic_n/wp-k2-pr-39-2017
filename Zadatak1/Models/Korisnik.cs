﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zadatak1.Models
{
    public enum polevi { MUSKI, ZENSKI }
    public enum ulogee { KORISNIK, ADMINISTRATOR }

    public class Korisnik
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public polevi Pol { get; set; }
        public string Email { get; set; }
        public DateTime datum_rodjenja { get; set; }
        public ulogee Uloga { get; set; }
        public bool LoggedIn { get; set; }

        public bool obrisan { get; set; }

        public Korisnik()
        {
            Username = "";
            Password = "";
            LoggedIn = false;
        }

        public Korisnik(string us,string pass , string name,string ln , polevi pol,string mejl,DateTime dr,ulogee u)
        {
            this.Username = us;
            this.Password = pass;
            this.Ime = name;
            this.Prezime = ln;
            this.Pol = pol;
            this.Email = mejl;
            this.datum_rodjenja = dr;
            this.Uloga = u;
            LoggedIn = false;
        }
       
        

    }
}