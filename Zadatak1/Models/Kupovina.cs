﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zadatak1.Models
{
    public class Kupovina
    {
        public Korisnik Kupac { get; set; }
        public Stan stan { get; set; }
        public DateTime Datum_Kupovine { get; set; }
        public int Naplacena_cena { get; set; }

        public Kupovina()
        {

        }
        public Kupovina(Korisnik k,Stan s,DateTime time,int cena)
        {
            this.Kupac = k;
            this.stan = s;
            this.Datum_Kupovine = time;
            this.Naplacena_cena = cena;
        }

    }
}