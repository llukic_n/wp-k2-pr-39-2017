﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Zadatak1.Models
{
    public enum Tip { GARSONJERA,DVOSOBAN,TROSOBAN,CETVOROSOBAN}
    public class Stan
    {
        public static int counter = 0;
        public int Id { get; set; }

        public string Drzava { get; set; }
        public string Grad { get; set; }
        public string Ulica_i_broj { get; set; }
        public int Sprat { get; set; }
        public int Kvadratura { get; set; }
        public string Opis { get; set; }
        public Tip Tip_stana { get; set; }
        public int Cena { get; set; }
        public bool NaProdaju { get; set; }
        

        //
        public Stan(string drzava,string grad,string ulica_i_broj,int sprat,int kvadratura,string opis,Tip type, int cena,bool naProdaju)
        {
            this.Id = counter++;
            this.Drzava = drzava;
            this.Grad = grad;
            this.Ulica_i_broj = ulica_i_broj;
            this.Sprat = sprat;
            this.Kvadratura = kvadratura;
            this.Opis = opis;
            this.Tip_stana = type;
            this.Cena = cena;
            this.NaProdaju = true;
        }

        public Stan(string drzava, string grad, string ulica_i_broj, int sprat, int kvadratura, string opis, Tip type, int cena)
        {
            this.Id = counter++;
            this.Drzava = drzava;
            this.Grad = grad;
            this.Ulica_i_broj = ulica_i_broj;
            this.Sprat = sprat;
            this.Kvadratura = kvadratura;
            this.Opis = opis;
            this.Tip_stana = type;
            this.Cena = cena;
            this.NaProdaju = true;
        }

        public Stan()
        {
            this.Id = counter++;
            this.NaProdaju = true;
        }



    }
}