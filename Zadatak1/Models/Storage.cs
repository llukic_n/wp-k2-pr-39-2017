﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace Zadatak1.Models
{
    public class Storage
    {
        public static List<Korisnik> UcitajKorisnika(string putanja)
        {
            List<Korisnik> povratna = new List<Korisnik>();
            putanja = HostingEnvironment.MapPath(putanja);

            FileStream fs = new FileStream(putanja,FileMode.Open);
            StreamReader sr = new StreamReader(fs);

            string admin = "";
            while ((admin = sr.ReadLine()) != null)
            {
                string[] polja = admin.Split(';');
                Enum.TryParse(polja[4], out polevi pomocni_pol);
                Enum.TryParse(polja[7], out ulogee pomocne_uloge);
                DateTime.TryParse(polja[6], out DateTime datum);

                Korisnik korisnik = new Korisnik(polja[0], polja[1], polja[2], polja[3], pomocni_pol, polja[5], datum, pomocne_uloge);
                povratna.Add(korisnik);
            }            

            sr.Close();
            fs.Close();

            return povratna;
        }

        public static List<Stan> UcitajStanove(string putanja)
        {
            List<Stan> stanovi = new List<Stan>();
            putanja = HostingEnvironment.MapPath(putanja);

            FileStream fs = new FileStream(putanja,FileMode.Open);
            StreamReader sr = new StreamReader(fs);

            string flats = "";
            while ((flats = sr.ReadLine())!= null)
            {
                string[] flats1 = new string[10];
                flats1 = flats.Split(';');
                Int32.TryParse(flats1[3], out int sprat);
                Int32.TryParse(flats1[4], out int kvadratura);
                Enum.TryParse(flats1[6], out Tip tip_Stana);
                Int32.TryParse(flats1[7], out int cena);
                bool.TryParse(flats1[8], out bool naProdaju);
                //
                Stan s = new Stan(flats1[0],flats1[1],flats1[2],sprat,kvadratura,flats1[5], tip_Stana, cena, naProdaju);
                stanovi.Add(s);

            }
            sr.Close();
            fs.Close();

            return stanovi;
        }

        public static void upisiKorisnika(Korisnik k)
        {
            string putanja = "~/App_Data/users.txt";
            putanja = HostingEnvironment.MapPath(putanja);

            FileStream fs = new FileStream(putanja, FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            string text = "";
            text += k.Username + ";" + k.Password + ";" + k.Ime + ";" + k.Prezime+";";
            text += k.Pol.ToString() + ";" + k.Email.ToString() + ";" + k.datum_rodjenja.ToString() + ";";
            text += k.Uloga.ToString();

            sw.WriteLine(text);

            //System.IO.File.WriteAllText(@"C:\Users\Lukic\source\repos\Kolokvijum\Zadatak1\App_Data\users.txt", text);
            sw.Close();
            fs.Close();
        }

        public static void upisiStan(Stan s)
        {
            string putanja = "~/App_Data/stanovi.txt";
            putanja = HostingEnvironment.MapPath(putanja);

            FileStream fs = new FileStream(putanja, FileMode.Append, FileAccess.Write);
            StreamWriter sw = new StreamWriter(fs);

            string text = "";
            text += s.Drzava + ";" + s.Grad + ";" + s.Ulica_i_broj + ";";
            text += s.Sprat.ToString() + ";" + s.Kvadratura.ToString() + ";";
            text += s.Opis + ";" + s.Tip_stana.ToString() + ";";
            text += s.Cena.ToString() + ";" + s.NaProdaju.ToString();

            sw.WriteLine(text);

            sw.Close();
            fs.Close();
        }

        public static void upisiSveStanove(List<Stan> stanovi)
        {
            //string putanja = "~/App_Data/stanovi.txt";
            //putanja = HostingEnvironment.MapPath(putanja);

            //FileStream fs = new FileStream(putanja, FileMode.Append, FileAccess.Write);
            //StreamWriter sw = new StreamWriter(fs);

            string text = "";
            foreach (Stan s in stanovi)
            {
                
                text += s.Drzava + ";" + s.Grad + ";" + s.Ulica_i_broj + ";";
                text += s.Sprat.ToString() + ";" + s.Kvadratura.ToString() + ";";
                text += s.Opis + ";" + s.Tip_stana.ToString() + ";";
                text += s.Cena.ToString() + ";" + s.NaProdaju.ToString() + "\n";

                //sw.WriteLine(text);
            }

            System.IO.File.WriteAllText(@"C:\Users\Lukic\source\repos\Kolokvijum\Zadatak1\App_Data\stanovi.txt", text);

            //sw.Close();
            //fs.Close();

        }


    }
}